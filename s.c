
/* 
   c program:
   betm = Binary Escape Time Mandelbrot sets
   
   --------------------------------
   1. draws Mandelbrot set for Fc(z)=z*z +c
   using Mandelbrot algorithm ( boolean escape time )
   -------------------------------         
   2. technique of creating ppm file is  based on the code of Claudio Rocchini
   http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
   create 24 bit color graphic file ,  portable pixmap file = PPM 
   see http://en.wikipedia.org/wiki/Portable_pixmap
   to see the file use external application ( graphic viewer)
-----
 it is example  for : 
 https://www.math.univ-toulouse.fr/~cheritat/wiki-draw/index.php/Mandelbrot_set
 
 -------------
 compile : 

 
 
   gcc s.c -lm -Wall
 
 
   ./a.out
   
   
   -------- git --------

cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-parallel.git
git add .
git commit -m "Initial commit"
git push -u origin master



 
 
 
*/
#include <stdio.h>
#include <math.h>
#include <complex.h> // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
 
 

 
 
 
 
/* screen ( integer) coordinate */
int iX,iY;
const int iXmax = 10000; 
const int iYmax = 10000;
/* world ( double) coordinate = parameter plane*/
double Cx,Cy;
const double CxMin=-2.2;
const double CxMax= 0.8;
const double CyMin=-1.5;
const double CyMax= 1.5;
/* */
double PixelWidth; //=(CxMax-CxMin)/iXmax;
double PixelHeight; // =(CyMax-CyMin)/iYmax;
/* color component ( R or G or B) is coded from 0 to 255 */
/* it is 24 bit color RGB file */
const int MaxColorComponentValue=255; 
FILE * fp;
char *filename="s.ppm"; 
char *comment="# ";/* comment should start with # */
        
static unsigned char color[3]; // 24-bit rgb color
/* Z=Zx+Zy*i  ;   Z0 = 0 */
double Zx, Zy;
double Zx2, Zy2; /* Zx2=Zx*Zx;  Zy2=Zy*Zy  */
/*  */
int Iteration;
const int IterationMax=2000;
/* bail-out value , radius of circle ;  */
const double EscapeRadius=2;
//double ER2=EscapeRadius*EscapeRadius;
        
        
        
        
 double complex give_c(int iX, int iY){
  double Cx,Cy;
  Cy=CyMin + iY*PixelHeight;
  if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
  Cx=CxMin + iX*PixelWidth;
   
  return Cx+Cy*I;
 
 
}
 
 




 int iterate(double complex C , int iMax)
  {
   int i;
   double complex Z= 0.0; // initial value for iteration Z0
   
   for(i=0;i<iMax;i++)
    {
      Z=Z*Z+C; // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
      if(cabs(Z)>EscapeRadius) break;
    }
   return i; 
 }
 
 
 
 
 
int compute_color(complex double c, unsigned char color[3]){
 
   int i;
   
   i = iterate( c, IterationMax);
  
    
  // boolean or binary escape time 
  if (i==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      color[0]=0;
      color[1]=0;
      color[2]=0;                           
    }
  else 
    { /* exterior of Mandelbrot set = white  */
      color[0]=255; /* Red*/
      color[1]=255;  /* Green */ 
      color[2]=255;/* Blue */
    };
 
   
  return 0;
}
 
 
 
 void setup(){
 
  //
  PixelWidth=(CxMax-CxMin)/iXmax;
  PixelHeight=(CyMax-CyMin)/iYmax;
        
         
  /*create new file,give it a name and open it in binary mode  */
  fp= fopen(filename,"wb"); /* b -  binary mode */
  /*write ASCII header to the file*/
  fprintf(fp,"P6\n %s\n %d\n %d\n %d\n",comment,iXmax,iYmax,MaxColorComponentValue);
 
 }
 
 
 
 void close(){
 fclose(fp);
 // info 
 printf("file %s saved \n", filename);
 
 
 }
 
 
 
int main()
{
        
  complex double c;
        
        
 
  setup();      
        
        
  /* render = compute and write image data bytes to the file*/
  for(iY=0;iY<iYmax;iY++)
    for(iX=0;iX<iXmax;iX++)
      { // compute pixel coordinate        
	c = give_c(iX, iY);  
	/* compute  pixel color (24 bit = 3 bytes) */
	compute_color(c,color);         
	/*write color to the file*/
	fwrite(color,1,3,fp);
      }
        
  
  
  close();
  
        
  return 0;
}
