== task:
* speed up the c code 
* code should be easy to read 
  * withoout changing the c code
  * adding minimal part of OpenMP/OpenACC code = Instruction-level parallelism

== Number of CPU cores
```bash
grep 'processor.*:' /proc/cpuinfo | wc -l 
8
```


== files
* sequential write to the file 
  * s.c :  without OpenMP                  : real =       8m40.032s = 520 s
  * s_omp.c : with OpenMP without order    : real =       0m20.218s =  20 s( 26 times faster but bad image !!!)
  * s_omp_order : with OpenMP with order=                 8m44.801s ( image is good but speed is the same !!!)
  * a.c  array without omp =                              8m34.799s 
  * o.c	 omp for dynamic without privite, bad image       0m16.398s 
  * o.c omp for schedule dynamic with privite, good image 1m50.751s 




== Git

```git
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-parallel.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

== Parallelisation
Instruction-level parallelism
* OpenMP
  ** GoMP https://gcc.gnu.org/projects/gomp/
  ** https://gcc.gnu.org/wiki/Offloading
* OpenACC


automatic-parallelization : https://en.wikipedia.org/wiki/Automatic_parallelization_tool
* gcc -floop-parallelize-all -ftree-parallelize-loops : https://gcc.gnu.org/wiki/AutoParInGCC
  **  https://gcc.gnu.org/wiki/Graphite/Parallelization
* PIPS : https://pips4u.org/
* par4all : https://github.com/Par4All/par4all



If you are not using graphite, use : -O2 -ftree-parallelize-loops=4 
If you are using graphite, you could use the flags: -O2 -ftree-parallelize-loops=4 -floop-parallelize-all 






Manual parallelization
* 


Benchmarks
* http://benchmarksgame.alioth.debian.org/
* https://wiki.haskell.org/Benchmarks_Game/Parallel/Mandelbrot
* http://distrustsimplicity.net/articles/mandelbrot-speed-comparison/


== Vectorisation = Array_programming ???? is it easy to do ?




== Tips:
*  each point is calculated independently = an embarrassingly parallel workload or problem (also called perfectly parallel or pleasingly parallel) is one where little or no effort is needed to separate the problem into a number of parallel tasks
* "Computing the Mandelbrot set is a canonical example of a badly balanced problem. I would therefore add schedule(dynamic[,something_small]) to the parallel for in mandelbrotSetCount." – Hristo Iliev 
* "Mandelbrot works best with both loops collapsed and schedule(dynamic)"
* writing to the file: 
  * "If you're writing to a file sequentially then you can use the ordered pragma before you write to the file. "
  * "The optimal solutions is to write to a memory buffer first and after the mandelbrot code finishes filling the buffer then write to a file"
  
  
  
== Links
* http://bisqwit.iki.fi/story/howto/openmp/#ExampleCalculatingTheMandelbrotFractalInParallel
* https://github.com/leVirve/Parallel-Mandelbrot-Set/blob/master/src/mandelbrot-openmp-dynamic.cpp 
  
