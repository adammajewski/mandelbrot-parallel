
/* 
   c program:
   betm = Binary Escape Time Mandelbrot sets
   
   --------------------------------
   1. draws Mandelbrot set for Fc(z)=z*z +c
   using Mandelbrot algorithm ( boolean escape time )
   -------------------------------         
   2. technique of creating ppm file is  based on the code of Claudio Rocchini
   http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
   create 24 bit color graphic file ,  portable pixmap file = PPM 
   see http://en.wikipedia.org/wiki/Portable_pixmap
   to see the file use external application ( graphic viewer)
-----
 it is example  for : 
 https://www.math.univ-toulouse.fr/~cheritat/wiki-draw/index.php/Mandelbrot_set
 
 -------------
 compile : 

 
 
   gcc o.c -lm -Wall -fopenmp
 
 
   ./a.out
   
   
   -------- git --------

cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-parallel.git
git add .
git commit -m "Initial commit"
git push -u origin master


  100 x   100 = 0 m  0.053s
 1000 x  1000 = 0 m  5.201s
10000 x 10000 = 8 m 34.799s 


 
 
 
*/
#include <stdio.h>
#include <stdlib.h>		// malloc
#include <math.h>
#include <complex.h> // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
#include <omp.h> 
 

 
 
 
 
/* screen ( integer) coordinate */

const int iWidth  = 10000; 
const int iHeight = 10000;
size_t MemmorySize;
/* world ( double) coordinate = parameter plane*/
// double complex C =  Cx + Cy*I ;
const double CxMin=-2.2;
const double CxMax= 0.8;
const double CyMin=-1.5;
const double CyMax= 1.5;
/* */
double PixelWidth; //=(CxMax-CxMin)/iWidth;
double PixelHeight; // =(CyMax-CyMin)/iHeight;
/* color component ( R or G or B) is coded from 0 to 255 */
/* it is 24 bit color RGB file */


int ColorBytes = 3; // 3*8 = 24 bit color        

/*  */
const int IterationMax=2000;

/* bail-out value , radius of circle ;  */
const double EscapeRadius=2.0;

       
// memmory virtual 1D array 
unsigned char *data;       
       
       
        
/* 
gives position ( index) in 1D virtual array  of 2D point (iX,iY) from ; uses also global variable iWidth 
without bounds check !!
*/
int f(int ix, int iy)
{ return ColorBytes*(ix + iy*iWidth); }
        
        
        
 double complex give_c(int iX, int iY){
  double Cx,Cy;
  Cy=CyMin + iY*PixelHeight;
  if (fabs(Cy)< PixelHeight/2) Cy=0.0; /* Main antenna */
  Cx=CxMin + iX*PixelWidth;
   
  return Cx+Cy*I;
 
 
}
 
 




 int iterate(double complex C , int iMax)
  {
   int i;
   double complex Z= 0.0; // initial value for iteration Z0
   
   for(i=0;i<iMax;i++)
    {
      Z=Z*Z+C; // https://stackoverflow.com/questions/6418807/how-to-work-with-complex-numbers-in-c
      if(cabs(Z)>EscapeRadius) break;
    }
   return i; 
 }
 
 
 
 
 
int ComputeAndSaveColor(int iX, int iY){
 
   int i;
   int Iteration;
   complex double c;
   
   c = give_c(iX, iY);  
   Iteration = iterate( c, IterationMax);
   i = f(iX, iY);  
    
  // boolean or binary escape time 
  if (Iteration==IterationMax)
    { /*  interior of Mandelbrot set = black  */
      data[i]=0;
      data[i+1]=0;
      data[i+2]=0;                           
    }
  else 
    { /* exterior of Mandelbrot set = white  */
      data[i]=255; /* Red*/
      data[i+1]=255;  /* Green */ 
      data[i+2]=255;/* Blue */
    };
 
   
  return 0;
}
 
 
 
int setup(){
 
  //
  PixelWidth=(CxMax-CxMin)/iWidth;
  PixelHeight=(CyMax-CyMin)/iHeight;
  //
  MemmorySize = iWidth * iHeight * ColorBytes * sizeof (unsigned char);	// https://stackoverflow.com/questions/492384/how-to-find-the-sizeof-a-pointer-pointing-to-an-array
        
  /* create dynamic 1D arrays for colors   */
  data = malloc (MemmorySize);
  if (data == NULL )
    { fprintf (stderr, " Error: Could not allocate memory !!! \n"); return 1; }

  printf (" No errors. End of setup \n");
  return 0;

 }
 
 
 
 
 
 // save dynamic "A" array to pgm file 
int SaveArray_2_PPM_file (unsigned char A[])
{

  FILE *fp;
  const unsigned int MaxColorComponentValue = 255;	/* color component is coded from 0 to 255 ;  it is 8 bit color file */
  char *filename = "op.ppm";
  char *comment = "# ";		/* comment should start with # */

  /* save image to the pgm file  */
  fp = fopen (filename, "wb");	/*create new file,give it a name and open it in binary mode  */
  fprintf (fp, "P6\n %s\n %u %u\n %u\n", comment, iWidth, iHeight, MaxColorComponentValue);	/*write header to the file */
  fwrite (A, MemmorySize, 1, fp);	/*write A array to the file in one step */
  printf ("File %s saved. \n", filename);
  fclose (fp);

  return 0;
}


 
 
 
 
 
 
 
 
void close(){
 SaveArray_2_PPM_file (data);
 // info 
 free(data); 
  
 
 }
 
 
 
int main()
{
  int iX,iY; // screen = integer coordinate in pixels       
 
        
        
 
  setup();      
        
  #pragma omp parallel for schedule(dynamic)  private(iX,iY)    
  // render image = scanline 2D   
  for(iY=0;iY<iHeight;iY++)
    for(iX=0;iX<iWidth;iX++)
      	ComputeAndSaveColor(iX, iY);         
	
      
        
  
  
  close();
  
        
  return 0;
}
